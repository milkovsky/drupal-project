<?php

// Include current directory. Will add policy.drush.inc.
$options['include'][] = __DIR__;

/**
 * List of tables whose *data* is skipped by the 'sql-dump' and 'sql-sync'
 * commands when the "--structure-tables-key=common" option is provided.
 * You may add specific tables to the existing array or add a new element.
 */
$options['structure-tables']['common'] = [
  'cache*',
  'history',
  'sessions',
  'watchdog',
];

$options['shell-aliases']['composer'] = "!composer --working-dir=$(drush dd)/../ ";
$options['shell-aliases']['deploy'] = "!drush composer install && drush entity-updates -y && drush updatedb -y && drush cim -y && drush cr";
$dsi_base = "drush site-install --account-name={$_ENV['DRUPAL_ACCOUNT_LOGIN']} --account-pass={$_ENV['DRUPAL_ACCOUNT_PASS']} --account-mail={$_ENV['DRUPAL_ACCOUNT_MAIL']} -y --config-dir=../config/sync {$_ENV['DRUPAL_INSTALL_PROFILE']}";
$options['shell-aliases']['dsi'] = "!chmod +w sites/default/settings.php; drush sql-create -y && $dsi_base";
