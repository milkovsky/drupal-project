<?php

/**
 * @file
 * Production environment configuration file.
 */

include $app_root . '/' . $site_path . '/settings.common.php';

$config['config_split.config_split.prod']['status'] = TRUE;
$settings['install_profile'] = $_ENV['DRUPAL_INSTALL_PROFILE'];
