<?php

/**
 * @file
 * Drupal dev environment configuration file.
 */

// Include the common settings file.
include $app_root . '/' . $site_path . '/settings.common.php';
$settings['install_profile'] = $_ENV['DRUPAL_INSTALL_PROFILE'];

$config['config_split.config_split.dev']['status'] = TRUE;

// Disable CSS and JS aggregation.
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

// Disable emails sending.
$config['system.mail']['interface']['default'] = 'devel_mail_log';
$config['devel.settings']['debug_mail_directory'] = 'temporary://devel_mail_log';

// Include local settings overrides.
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}
