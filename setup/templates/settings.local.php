<?php

/**
 * @file
 * Local environment configuration file.
 */

/* Disable caching.
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
//*/

/* Enable CSS and JS aggregation.
$config['system.performance']['css']['preprocess'] = TRUE;
$config['system.performance']['js']['preprocess'] = TRUE;
//*/

//$config['system.logging']['error_level'] = 'verbose';
