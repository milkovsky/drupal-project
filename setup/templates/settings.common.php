<?php

/**
 * @file
 * Common configuration file related to every environment.
 */

$config_directories = array();
$config_directories[CONFIG_SYNC_DIRECTORY] = $_ENV['DRUPAL_CONFIG_SYNC_DIRECTORY'];

$settings['install_profile'] = $_ENV['DRUPAL_INSTALL_PROFILE'];
$settings['hash_salt'] = $_ENV['DRUPAL_HASH_SALT'];

$settings['file_private_path'] = $_ENV['DRUPAL_FILE_PRIVATE_PATH'];
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

// Always set the fast backend for bootstrap, discover and config, otherwise
// this gets lost when redis is enabled.
$settings['cache']['bins']['bootstrap'] = 'cache.backend.chainedfast';
$settings['cache']['bins']['discovery'] = 'cache.backend.chainedfast';
$settings['cache']['bins']['config'] = 'cache.backend.chainedfast';

$databases = array();
$databases['default']['default'] = array (
  'database' => $_ENV['DRUPAL_DB_DATABASE'],
  'username' => $_ENV['DRUPAL_DB_USER'],
  'password' => $_ENV['DRUPAL_DB_PASS'],
  'prefix' => '',
  'host' => $_ENV['DRUPAL_DB_HOST'],
  'port' => $_ENV['DRUPAL_DB_PORT'],
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
