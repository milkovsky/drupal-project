#!/usr/bin/env bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "`dirname $0`"

if [ ! -f ${DIR}/vendor/drush/drush/drush.launcher ]; then
  echo >&2 "Drush was not found in this project's vendor directory. You can install it by typing:"
  echo >&2 "composer install"
  exit 1
fi

bin/drush.launcher --root=${DIR}/docroot --config=${DIR}/drush/drushrc.php --alias-path=${DIR}/drush/site-aliases "$@"
